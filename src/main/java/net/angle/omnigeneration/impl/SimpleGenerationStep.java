package net.angle.omnigeneration.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
public abstract class SimpleGenerationStep<T, E> extends AbstractGenerationStep<T, E> {
    private final @Getter List<GenerationStep<T, E>> dependentSteps = new ArrayList<>();
    
    public void addDependentStep(GenerationStep<T, E> step) {
        dependentSteps.add(step);
    }

    @Override
    public void beginNextSteps(GenerationRequest<T, E> request) {
        for (GenerationStep<T, E> dependentStep : getDependentSteps())
            if (!request.getCompletedSteps().contains(dependentStep) && dependentStep.dependenciesAreComplete(request)) {
                request.submitTicket(dependentStep.getTicket(request));
            }
    }
}