package net.angle.omnigeneration.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import net.angle.omnigeneration.api.GenerationTicket;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
public abstract class AbstractGenerationStep<T, E> implements GenerationStep<T, E> {
    private final @Getter List<Dependency<T, E>> dependencies = new ArrayList<>();
    
    @Override
    public void addDependency(Dependency<T, E> step) {
        dependencies.add(step);
    }

    @Override
    public void init() {}

    @Override
    public GenerationTicket<T, E> getTicket(GenerationRequest<T, E> request) {
        return new BasicGenerationTicket<T, E>(this, request);
    }
}