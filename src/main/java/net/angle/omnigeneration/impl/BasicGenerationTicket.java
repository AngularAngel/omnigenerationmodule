package net.angle.omnigeneration.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.GenerationTicket;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicGenerationTicket<T, E> implements GenerationTicket<T, E> {
    private final @Getter GenerationStep<T, E> step;
    private final @Getter GenerationRequest<T, E> request;
    
    public void process() {
        step.process(request);
    }

    @Override
    public void run() {
        try {
            if (!step.dependenciesAreComplete(request) || !request.startBlocking(step)) {
                request.getGenerationManager().submitTicket(this);
                return;
            }
            request.removeWaitingStep(step);
            process();
            request.addCompletedStep(step);
            
            step.beginNextSteps(request);
            request.stopBlocking();
        } catch(Exception ex) {
            Logger.getLogger(BasicGenerationTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}