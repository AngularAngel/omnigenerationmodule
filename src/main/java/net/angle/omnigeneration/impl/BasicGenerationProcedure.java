package net.angle.omnigeneration.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationProcedure;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public abstract class BasicGenerationProcedure<T, E> implements GenerationProcedure<T, E> {
    private final @Getter List<GenerationStep<T, E>> steps = new ArrayList<>();
    private final @Getter List<GenerationStep<T, E>> initialSteps = new ArrayList<>();
    private final @Getter GenerationManager generationManager;

    @Override
    public void addStep(GenerationStep<T, E> step) {
        steps.add(step);
        if (step.getDependencies().isEmpty())
            initialSteps.add(step);
    }

    @Override
    public void removeStep(GenerationStep<T, E> step) {
        steps.remove(step);
        if (initialSteps.contains(step))
            initialSteps.remove(step);
    }

    @Override
    public void init() {
        for (GenerationStep step : steps)
            step.init();
    }

    @Override
    public void beginGeneration(GenerationRequest<T, E> request) {
        generationManager.addRequest(request);
        for (GenerationStep<T, E> step : initialSteps)
            generationManager.submitTicket(new BasicGenerationTicket(step, request));
    }
}