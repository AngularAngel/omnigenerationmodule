package net.angle.omnigeneration.impl;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.GenerationTicket;
import net.angle.omnigeneration.api.process.GenerationRequest;

/**
 *
 * @author angle
 */
public class BasicGenerationManager<T, E> implements GenerationManager<T, E> {
    private final HashMap<E, GenerationRequest<T, E>> ongoingRequests = new HashMap<>();
    private final ExecutorService executorService;
    
    public BasicGenerationManager(int threads) {
        executorService = Executors.newFixedThreadPool(threads,
                new ThreadFactoryBuilder().setNameFormat("Generation Thread %d").build()
        );
    }
    
    public BasicGenerationManager() {
        this(Math.max(Runtime.getRuntime().availableProcessors() - 2, 1));
    }

    @Override
    public void addRequest(GenerationRequest<T, E> request) {
        ongoingRequests.put(request.getIdentifier(), request);
    }

    @Override
    public void removeRequest(GenerationRequest<T, E> request) {
        ongoingRequests.remove(request.getIdentifier(), request);
    }

    @Override
    public void removeRequest(E identifier) {
        ongoingRequests.remove(identifier);
    }

    @Override
    public GenerationRequest<T, E> getRequest(E identifier) {
        return ongoingRequests.get(identifier);
    }
    
    @Override
    public void submitTicket(GenerationTicket<T, E> ticket) {
        try {
            executorService.submit(ticket);
        } catch(RejectedExecutionException ex) {
            if (!executorService.isShutdown())
                Logger.getLogger(BasicGenerationManager.class.getName()).log(Level.SEVERE, "Rejected execution while not shutting down!", ex);
        }
    }

    @Override
    public void shutdown() {
        executorService.shutdown();
    }
}