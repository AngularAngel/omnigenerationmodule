package net.angle.omnigeneration.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
public abstract class AbstractGenerationRequest<T, E> implements GenerationRequest<T, E> {
    private final @Getter GenerationManager<T, E> generationManager;
    private final @Getter List<GenerationStep<T, E>> completedSteps = new ArrayList<>();
    private volatile@Getter GenerationStep<T, E> currentStep;
    private final @Getter List<GenerationStep<T, E>> waitingSteps = new ArrayList<>();

    public AbstractGenerationRequest(GenerationManager<T, E> generationManager) {
        this.generationManager = generationManager;
    }
    
    @Override
    public synchronized boolean startBlocking(GenerationStep<T, E> step) {
        if (Objects.isNull(currentStep)) {
            currentStep = step;
            return true;
        } else
            return false;
    }
    
    @Override
    public synchronized void stopBlocking() {
        currentStep = null;
    }
    
    @Override
    public void addCompletedStep(GenerationStep<T, E> step) {
        completedSteps.add(step);
    }
    
    @Override
    public void removeCompletedStep(GenerationStep<T, E> step) {
        completedSteps.remove(step);
    }
    
    @Override
    public void addWaitingStep(GenerationStep<T, E> step) {
        waitingSteps.add(step);
    }
    
    @Override
    public void removeWaitingStep(GenerationStep<T, E> step) {
        waitingSteps.remove(step);
    }
    
    @Override
    public void nextStep(GenerationStep<T, E> nextStep) {
        if (!getCompletedSteps().contains(nextStep)) {
            submitTicket(new BasicGenerationTicket(nextStep, this));
        }
    }
}