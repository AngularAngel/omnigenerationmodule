package net.angle.omnigeneration.api;

import net.angle.omnigeneration.api.process.GenerationRequest;

/**
 *
 * @author angle
 */
public interface GenerationManager<T, E> {
    public void addRequest(GenerationRequest<T, E> request);
    public void removeRequest(GenerationRequest<T, E> request);
    public void removeRequest(E identifier);
    public GenerationRequest<T, E> getRequest(E identifier);
    public void submitTicket(GenerationTicket<T, E> ticket);
    public void shutdown();
}