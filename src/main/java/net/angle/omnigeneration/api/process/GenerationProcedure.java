package net.angle.omnigeneration.api.process;

import java.util.List;
import net.angle.omnigeneration.api.GenerationManager;

/**
 *
 * @author angle
 */
public interface GenerationProcedure<T, E> {
    public GenerationManager getGenerationManager();
    public void init();
    public List<GenerationStep<T, E>> getSteps();
    public void addStep(GenerationStep<T, E> step);
    public void removeStep(GenerationStep<T, E> step);
    public void beginGeneration(GenerationRequest<T, E> request);
    public void resubmit(GenerationRequest<T, E> request);
}