package net.angle.omnigeneration.api.process;

import java.util.List;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.api.GenerationTicket;

/**
 *
 * @author angle
 */
public interface GenerationStep<T, E> {
    public void init();
    
    public List<Dependency<T, E>> getDependencies();
    
    public void addDependency(Dependency<T, E> dependency);
    public default void addDependency(GenerationStep<T, E> step) {
        addDependency(new LocalStepDependency<>(step));
    }
    
    public GenerationTicket<T, E> getTicket(GenerationRequest<T, E> request);
    
    public default boolean dependenciesAreComplete(GenerationRequest<T, E> request) {
        for (Dependency<T, E> dependency : getDependencies())
            if (!dependency.isCompleted(request))
                return false;
        return true;
    }
    
    public void process(GenerationRequest<T, E> request);
    
    public void beginNextSteps(GenerationRequest<T, E> request);
    
    @FunctionalInterface
    public static interface Dependency<T, E> {
        public boolean isCompleted(GenerationRequest<T, E> request);
    }
    
    @RequiredArgsConstructor
    public static class LocalStepDependency<T, E> implements Dependency<T, E> {
        private final GenerationStep<T, E> step;
        
        @Override
        public boolean isCompleted(GenerationRequest<T, E> request) {
            return request.getCompletedSteps().contains(step);
        }
    }
}