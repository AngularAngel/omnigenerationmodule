package net.angle.omnigeneration.api.process;

import java.util.List;
import java.util.Objects;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.GenerationTicket;

/**
 *
 * @author angle
 */
public interface GenerationRequest<T, E> {
    public E getIdentifier();
    public boolean startBlocking(GenerationStep<T, E> step);
    public void stopBlocking();
    public List<GenerationStep<T, E>> getCompletedSteps();
    public void addCompletedStep(GenerationStep<T, E> step);
    public void removeCompletedStep(GenerationStep<T, E> step);
    public GenerationStep<T, E> getCurrentStep();
    public List<GenerationStep<T, E>> getWaitingSteps();
    public void addWaitingStep(GenerationStep<T, E> step);
    public void removeWaitingStep(GenerationStep<T, E> step);
    public T getResult();
    public void setResult(T result);
    public GenerationManager<T, E> getGenerationManager();
    public default void submitTicket(GenerationTicket<T, E> ticket) {
        getGenerationManager().submitTicket(ticket);
        addWaitingStep(ticket.getStep());
    }
    public void nextStep(GenerationStep<T, E> nextStep);
    public default boolean isOngoing() {
        return Objects.nonNull(getCurrentStep()) || !getWaitingSteps().isEmpty();
    }
    public default boolean isComplete() {
        return !isOngoing();
    }
}