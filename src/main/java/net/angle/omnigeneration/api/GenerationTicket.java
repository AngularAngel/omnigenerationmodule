package net.angle.omnigeneration.api;

import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;

/**
 *
 * @author angle
 */
public interface GenerationTicket<T, E> extends Runnable {
    public GenerationStep<T, E> getStep();
    public GenerationRequest<T, E> getRequest();
}