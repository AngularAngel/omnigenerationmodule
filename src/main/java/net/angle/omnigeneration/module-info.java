
module omni.generation {
    requires static lombok;
    requires com.google.common;
    requires java.logging;
    requires devil.util;
    exports net.angle.omnigeneration.api;
    exports net.angle.omnigeneration.api.process;
    exports net.angle.omnigeneration.impl;
}